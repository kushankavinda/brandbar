import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MyPostedAddsService {

  options = { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) };
  
  constructor(private httpclient: HttpClient) { }

  GetUserPostedAdds (currentUserEmail): Observable <any> {
    return this.httpclient.get<any>('https://159.65.148.244:20000/api/addPost/user-posted-adds?useremail='+currentUserEmail,  this.options);
      //  .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }
  
  removeByPublisher(id): Observable <any> {
    return this.httpclient.delete<any>('https://159.65.148.244:20000/api/addPost/publisher-delete-add/{id}?id='+id,  this.options);
      //  .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }
}
