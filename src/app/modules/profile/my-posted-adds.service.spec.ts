import { TestBed } from '@angular/core/testing';

import { MyPostedAddsService } from './my-posted-adds.service';

describe('MyPostedAddsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MyPostedAddsService = TestBed.get(MyPostedAddsService);
    expect(service).toBeTruthy();
  });
});
