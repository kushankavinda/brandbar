import { Component, OnInit } from '@angular/core';
import { AuthService, GoogleLoginProvider, FacebookLoginProvider, SocialUser } from 'angularx-social-login';
import { MyPostedAddsService } from '../my-posted-adds.service';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.css'],
})
export class LoginFormComponent implements OnInit {



  constructor(private _myPostedAddsService: MyPostedAddsService ) {}


  ngOnInit() {
    this.UserPostedAdds();
  }

  adds: any;

  UserPostedAdds () {
    this.adds = [];
    const currentUserEmail = localStorage.getItem('useremail');

    this._myPostedAddsService.GetUserPostedAdds(encodeURIComponent(currentUserEmail)).subscribe(
      res => {
        res.forEach(
          event => {
            console.log(event);

            this.adds.push(event);
          }
        );
      }
    );
  }

  deleteThisAdd(itemId) {
    this._myPostedAddsService.removeByPublisher(itemId).subscribe(
      res => {
        console.log(res);
      }
    );
  }

}
