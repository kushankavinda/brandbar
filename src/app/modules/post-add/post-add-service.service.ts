import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { PostAdd } from './post-add';
 // import 'rxjs/add/operator/catch';

@Injectable({
  providedIn: 'root'
})
export class PostAddServiceService {

  constructor( private httpclient: HttpClient) { }

  options = { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) };

  postAdd (postAdd: PostAdd): Observable <any> {

    const objectjson = JSON.stringify(postAdd);
    return this.httpclient.post('https://159.65.148.244:20000/api/addPost', objectjson, this.options);
      //  .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }

  /*saveImage (a , file ){


    var xhr = new XMLHttpRequest();
    xhr.withCredentials = true;

    xhr.addEventListener("readystatechange", function() {
      if(this.readyState === 4) {
        console.log(this.responseText);
      }
    });

    xhr.open("POST", "http://159.65.148.244:20000/api/addPost/photos/add");
    xhr.setRequestHeader("Content-Type", "multipart/form-data; boundary=--------------------------648820239765328290769787");

    xhr.send(data);

  }*/

  uploadFileToUrl(file,fileSubject): Observable<any>{

   return this.httpclient.post<any>("https://159.65.148.244:20000/api/addPost/photos/add?title="+fileSubject, file);


  }

  handleError(err: HttpErrorResponse) {
    return Observable.throw(err.message);
  }

}
