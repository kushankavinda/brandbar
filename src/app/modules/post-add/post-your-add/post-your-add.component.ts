import { Component, OnInit, ViewChild } from '@angular/core';
import { SelectItem } from 'primeng/api';
import {InputTextareaModule} from 'primeng/inputtextarea';
import { PostAddServiceService } from '../post-add-service.service';
import { PostAdd } from '../post-add';
import { Router } from '@angular/router';
import { analyzeAndValidateNgModules } from '@angular/compiler';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-post-your-add',
  templateUrl: './post-your-add.component.html',
  styleUrls: ['./post-your-add.component.css']
})

export class PostYourAddComponent implements OnInit {

//  post: any;

  cat: any;
  description: any;
  subject: any;
  phone: any;
  cities1: SelectItem[];
  address: any;
  price: any;
  purchase: any;

  isLoggedIn: boolean;
  constructor( private _postAddServiceService: PostAddServiceService , private router: Router ,private formBuilder: FormBuilder ) {
       // SelectItem API with label-value pairs
       this.cities1 = [
        {label:'Select District', value:null},
        {label:'Ampara', value:{id:1, name: 'Ampara', code: 'Ampara'}},
        {label:'Anuradhapura', value:{id:2, name: 'Anuradhapura', code: 'Anuradhapura'}},
        {label:'Badulla', value:{id:3, name: 'Badulla', code: 'Badulla'}},
        {label:'Batticaloa', value:{id:4, name: 'Batticaloa', code: 'Batticaloa'}},
        {label:'Colombo', value:{id:5, name: 'Colombo', code: 'Colombo'}},
        {label:'Galle', value:{id:5, name: 'Galle', code: 'Galle'}},
        {label:'Gampaha', value:{id:5, name: 'Gampaha', code: 'Gampaha'}},
        {label:'Hambantota', value:{id:5, name: 'Hambantota', code: 'Hambantota'}},
        {label:'Jaffna', value:{id:5, name: 'Jaffna', code: 'Jaffna'}},
        {label:'Kalutara', value:{id:5, name: 'Kalutara', code: 'Kalutara'}},
        {label:'Kandy', value:{id:5, name: 'Kandy', code: 'Kandy'}},
        {label:'Kegalle', value:{id:5, name: 'Kegalle', code: 'Kegalle'}},
        {label:'Kilinochchi', value:{id:5, name: 'Kilinochchi', code: 'Kilinochchi'}},
        {label:'Kurunegala', value:{id:5, name: 'Kurunegala', code: 'Kurunegala'}},
        {label:'Mannar', value:{id:5, name: 'Mannar', code: 'Mannar'}},
        {label:'Matale', value:{id:5, name: 'Matale', code: 'Matale'}},
        {label:'Matara', value:{id:5, name: 'Matara', code: 'Matara'}},
        {label:'Moneragala', value:{id:5, name: 'Moneragala', code: 'Moneragala'}},
        {label:'Mullaitivu', value:{id:5, name: 'Mullaitivu', code: 'Mullaitivu'}},
        {label:'Nuwara Eliya', value:{id:5, name: 'Nuwara Eliya', code: 'Nuwara Eliya'}},
        {label:'Polonnaruwa', value:{id:5, name: 'Polonnaruwa', code: 'Polonnaruwa'}},
        {label:'Puttalam', value:{id:5, name: 'Puttalam', code: 'Puttalam'}},
        {label:'Ratnapura', value:{id:5, name: 'Ratnapura', code: 'Ratnapura'}},
        {label:'Trincomalee', value:{id:5, name: 'Trincomalee', code: 'Trincomalee'}},
        {label:'Vavuniya', value:{id:5, name: 'Vavuniya', code: 'Vavuniya'}}
    ];
  }

  uploadForm: FormGroup; 
  ngOnInit() {
    this.uploadForm = this.formBuilder.group({
      profile: ['']
    });

    const currentUserEmail = localStorage.getItem('useremail');
    
    if(typeof currentUserEmail!='undefined' && currentUserEmail){
      this.isLoggedIn = false;
    }else {
      this.isLoggedIn = true;
    }
  }

  submitAdd() {
    const postAdd = new  PostAdd ('','','','','','','','','');

    postAdd.category =  this.cat.name;
    postAdd.description = this.description;
    postAdd.pushlisherRef = 'user';
    postAdd.subject = this.subject + "-Refrence"+Guid.newGuid();
    postAdd.phone = this.phone;
    postAdd.address = this.address;
    postAdd.price = this.price;
    postAdd.purchase = this.purchase;

    const currentUserEmail = localStorage.getItem('useremail');
    postAdd.publishedBy = currentUserEmail;


    
    this._postAddServiceService.postAdd(postAdd)
    .subscribe(
      res => {
          console.log(res);
      }
     );
     console.log("SAVE INPROGRESS");



    const formData = new FormData();
    formData.append('image', this.uploadForm.get('profile').value);
    this.fileUpload(formData ,postAdd.subject );



    

     this.router.navigate(['advertisment']);
  }


  onFileSelect(event) {
    if (event.target.files.length > 0) {
      const file = event.target.files[0];
      this.uploadForm.get('profile').setValue(file);
    }
  }


  fileUpload( file , fileSubject) {
    this._postAddServiceService.uploadFileToUrl(file,fileSubject)
    .subscribe(
      res => {
          console.log(res);
      }
     );
  }







}

class Guid {
  static newGuid() {
    return 'xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      var r = Math.random() * 16 | 0,
        v = c == 'x' ? r : (r & 0x3 | 0x8);
      return v.toString(16);
    });
  }
}