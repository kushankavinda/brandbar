export class PostAdd {
constructor (
        public category: string,
        public description: string,
        public pushlisherRef: string,
        public subject: string,
        public phone: string,
        public publishedBy: string,
        public address: string,
        public price: string,
        public purchase: string
) {}

}

