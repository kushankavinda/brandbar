import { Component, OnInit } from '@angular/core';
import { FullAddvertismentsService } from './full-addvertisments.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-full-addvertisment',
  templateUrl: './full-addvertisment.component.html',
  styleUrls: ['./full-addvertisment.component.css']
})
export class FullAddvertismentComponent implements OnInit {
  adds: any [] = [];

  routerUrl: any;
  saveAndCloseVisible: boolean;
  constructor(private router: Router ,
    private fullAddvertismentsService: FullAddvertismentsService) { }


    cards: any;

  ngOnInit() {

    this.cards = [
      {
        title: 'Card Title 1',
        description: 'Some quick example text to build on the card title and make up the bulk of the card content',
        buttonText: 'Button',
        img: 'https://mdbootstrap.com/img/Photos/Horizontal/Nature/4-col/img%20(34).jpg'
      },
      {
        title: 'Card Title 2',
        description: 'Some quick example text to build on the card title and make up the bulk of the card content',
        buttonText: 'Button',
        img: 'https://mdbootstrap.com/img/Photos/Horizontal/Nature/4-col/img%20(34).jpg'
      },
      {
        title: 'Card Title 3',
        description: 'Some quick example text to build on the card title and make up the bulk of the card content',
        buttonText: 'Button',
        img: 'https://mdbootstrap.com/img/Photos/Horizontal/Nature/4-col/img%20(34).jpg'
      },
      {
        title: 'Card Title 4',
        description: 'Some quick example text to build on the card title and make up the bulk of the card content',
        buttonText: 'Button',
        img: 'https://mdbootstrap.com/img/Photos/Horizontal/Nature/4-col/img%20(34).jpg'
      },];
  
      // adds array
    //  this.adds = ['aaa', 'bbb', 'c', 'd', 'e', 'f' , 'g' , 'h'];
    this.routerUrl = this.router.url;
    if ( this.routerUrl === '/advertisment') {
      this.timeline();
    } else if (this.routerUrl === '/askDiscount') {
      this.askDiscount();
    } else if (this.routerUrl === '/follow') {
      this.GetfollowAdds();
    }else if ( this.routerUrl === '') {
      this.timeline();
    }
  }

  Cancel(itemId: any) {
    //  this.adds.splice(i, 1);
    this.fullAddvertismentsService.removeFormTimeline(itemId).subscribe(
      res => {
        console.log(res);
        this.timeline();
      }
    );

  }
  Save(i: any, addvertisment: any) {
    this.adds.splice(0, 0, this.adds[i]);
    this.adds.splice( i + 1, 1);
    this.fullAddvertismentsService.saveFormTimeline(addvertisment).subscribe(
      res => {
      //  this.timeline();
      }
    );
  }


  Follow(itemId: any) {
    this.fullAddvertismentsService.followFormTimeline(itemId).subscribe(
      res => {
      //  this.timeline();
      }
    );
  }

 

  timeline () {
    this.adds = [];
    this.saveAndCloseVisible = true;

    this.fullAddvertismentsService.allAdd().subscribe(
      res => {
        res.forEach(
          event => {
            this.adds.push(event);
          }
        );
      }
    );
  }
  askDiscount () {
    this.adds = [];
    this.saveAndCloseVisible = false;

    this.fullAddvertismentsService.removeAdds().subscribe(
      res => {
        res.forEach(
          event => {
            this.adds.push(event);
          }
        );
      }
    );
  }
  GetfollowAdds () {
    this.adds = [];
    this.saveAndCloseVisible = false;
    this.fullAddvertismentsService.GetfollowAdds().subscribe(
      res => {
        res.forEach(
          event => {
            this.adds.push(event);
          }
        );
      }
    );
  }

}
