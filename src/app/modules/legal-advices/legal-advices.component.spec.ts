import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LegalAdvicesComponent } from './legal-advices.component';

describe('LegalAdvicesComponent', () => {
  let component: LegalAdvicesComponent;
  let fixture: ComponentFixture<LegalAdvicesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LegalAdvicesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LegalAdvicesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
