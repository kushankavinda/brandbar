import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import {AccordionModule} from 'primeng/accordion';     // accordion and accordion tab
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MenubarModule} from 'primeng/menubar';
import {CardModule} from 'primeng/card';
import { ButtonModule } from 'primeng/button';
import { AuthServiceConfig, GoogleLoginProvider,SocialLoginModule } from 'angularx-social-login';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { LegalAdvicesComponent } from '../legal-advices/legal-advices.component';
// import { AddvertismentModule } from '../addvertisment/addvertisment.module';

const config = new AuthServiceConfig([
  {
    id: GoogleLoginProvider.PROVIDER_ID,
    provider: new GoogleLoginProvider("341529775523-7uginb84ljg0rlc29b9s1ku3jl94ndd7.apps.googleusercontent.com"/*"349433778527-i90mcuoa0c9t42fam2623hror879ck82.apps.googleusercontent.com"*//*"341529775523-rgpil698plqk2q3pps42rt49gk2p26i1.apps.googleusercontent.com"*/)
  }
]);
export function provideConfig() {
  return config;
}

@NgModule({
  declarations: [
    AppComponent,
    LegalAdvicesComponent

  ],
  imports: [
    BrowserModule,
    AccordionModule,
    BrowserAnimationsModule,
    MenubarModule,
    CardModule,
    ButtonModule,
    ReactiveFormsModule,
    Ng2SearchPipeModule,
    FormsModule,
  //  AddvertismentModule,
  SocialLoginModule,
    AppRoutingModule
  ],
  providers: [
    {
      provide: AuthServiceConfig,
      useFactory: provideConfig
    }

  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
