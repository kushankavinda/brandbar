import { Component, OnInit } from '@angular/core';
import {MenuItem} from 'primeng/api';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService, GoogleLoginProvider, SocialUser } from 'angularx-social-login';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'BrandBar';
  items: MenuItem[];


  signinForm: FormGroup;
  user: SocialUser;
  loggedIn: boolean;


  searchText;
  
    constructor(private router: Router ,private fb: FormBuilder, private authService: AuthService) {}

    ngOnInit() {

        this.signinForm = this.fb.group({
            email: ['', Validators.required],
            password: ['', Validators.required]
          });  
          
          
        this.authService.authState.subscribe((user) => {
            this.user = user;
            this.loggedIn = (user != null);
            console.log(this.user);

            try{

            console.log(this.user.email);
            localStorage.setItem('useremail',this.user.email );

            }catch(e) {
                console.log(e);
            }
        
          });


        this.items = [
            {
                label: 'All Advertisments',
                command: () => this.navBarClick('advertisment')
                /*,
                icon: 'pi pi-fw pi-pencil',
                items: [
                    {label: 'Delete', icon: 'pi pi-fw pi-trash'},
                    {label: 'Refresh', icon: 'pi pi-fw pi-refresh'}
                ]*/
            },
            {
                label: 'Followed Addvertisments',
                command: () => this.navBarClick('follow'),
                icon: "pi pi-fw pi-calendar",
            },
            {
                label: 'Ask For a discount',
                command: () => this.navBarClick('askDiscount'),
                icon: "pi pi-fw pi-calendar-times",

            },
            {
                label: 'Posted Addvertisments',
                command: () => this.navBarClick('posted-adds')
            },
            {
                label: 'Post New Add',
                command: () => this.navBarClick('post-advertisment'),
           /*     style: { 'background-color': 'white', 'right': '20px','top':'6px','position':'absolute'} */
            }/*,
            {
                label: 'Post Advertisment',
                command: () => this.navBarClick('post-advertisment'),
                style: { 'background-color': 'coral', 'left': '10px'}
            }*/
        ];


    }


    testRoutine() {
        this.router.navigate(['/advertisment']);
    }

    postAdd() {
        this.router.navigate(['/post-advertisment']);
    }

    navBarClick(value: string) {
        this.router.navigate(['/' + value]);
    }


    

    signInWithGoogle(): void {

        this.authService.signIn(GoogleLoginProvider.PROVIDER_ID);

    }

    signOut(): void {

        localStorage.clear();
        this.authService.signOut();

    }
    



}
