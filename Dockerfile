FROM node:12.7-alpine AS build

# Create app directory
WORKDIR ../brandbar

# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where available (npm@5+)
COPY package*.json ./

RUN npm install
# If you are building your code for production
# RUN npm ci --only=production

# Bundle app source
COPY . .

EXPOSE 443
CMD [ "npm", "run", "start", "--ssl" , "--", "--port" ,"443" ,"--", "--host=0.0.0.0", "--disable-host-check"  ]


